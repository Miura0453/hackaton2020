#!/usr/bin/env python3

import io
import cv2 as cv
import os
import math
import imutils
import numpy as np
# import pytesseract
import pandas as pd
import fitz
import io
from pdf2image import convert_from_path
# import imageio
import re
from fuzzywuzzy import fuzz
import time
import unicodedata

from flask import jsonify
from flask import Flask, request

app = Flask(__name__)
#%%
from google.cloud import vision
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'MyProject24836-9cefd1a1f9ec.json'
client = vision.ImageAnnotatorClient()
#%%
def decode(scores, geometry, scoreThresh):
    detections = []
    confidences = []

    ############ CHECK DIMENSIONS AND SHAPES OF geometry AND scores ############
    assert len(scores.shape) == 4, "Incorrect dimensions of scores"
    assert len(geometry.shape) == 4, "Incorrect dimensions of geometry"
    assert scores.shape[0] == 1, "Invalid dimensions of scores"
    assert geometry.shape[0] == 1, "Invalid dimensions of geometry"
    assert scores.shape[1] == 1, "Invalid dimensions of scores"
    assert geometry.shape[1] == 5, "Invalid dimensions of geometry"
    assert scores.shape[2] == geometry.shape[2], "Invalid dimensions of scores and geometry"
    assert scores.shape[3] == geometry.shape[3], "Invalid dimensions of scores and geometry"
    height = scores.shape[2]
    width = scores.shape[3]
    for y in range(0, height):

        # Extract data from scores
        scoresData = scores[0][0][y]
        x0_data = geometry[0][0][y]
        x1_data = geometry[0][1][y]
        x2_data = geometry[0][2][y]
        x3_data = geometry[0][3][y]
        anglesData = geometry[0][4][y]
        for x in range(0, width):
            score = scoresData[x]

            # If score is lower than threshold score, move to next x
            if(score < scoreThresh):
                continue

            # Calculate offset
            offsetX = x * 4.0
            offsetY = y * 4.0
            angle = anglesData[x]

            # Calculate cos and sin of angle
            cosA = math.cos(angle)
            sinA = math.sin(angle)
            h = x0_data[x] + x2_data[x]
            w = x1_data[x] + x3_data[x]

            # Calculate offset
            offset = ([offsetX + cosA * x1_data[x] + sinA * x2_data[x], offsetY - sinA * x1_data[x] + cosA * x2_data[x]])

            # Find points for rectangle
            p1 = (-sinA * h + offset[0], -cosA * h + offset[1])
            p3 = (-cosA * w + offset[0],  sinA * w + offset[1])
            center = (0.5*(p1[0]+p3[0]), 0.5*(p1[1]+p3[1]))
            detections.append((center, (w,h), -1*angle * 180.0 / math.pi))
            confidences.append(float(score))

    # Return detections and confidences
    return [detections, confidences]

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii.decode("utf-8")

def correct(frame, avg_rot, rotate=False):
    sufix = ''
    if rotate == True:
        frame = imutils.rotate_bound(frame, avg_rot+90)
        sufix += '_rot'
    return frame, sufix

def get_lecture(frame):
    global data
    data=[]
    outputLayers = []
    outputLayers.append("feature_fusion/Conv_7/Sigmoid")
    outputLayers.append("feature_fusion/concat_3")
    height_ = frame.shape[0]
    width_ = frame.shape[1]
    rW = width_ / float(inpWidth)
    rH = height_ / float(inpHeight)

    blob = cv.dnn.blobFromImage(frame, 1.0, (inpWidth, inpHeight), (123.68, 116.78, 103.94), True, False)

    net.setInput(blob)
    output = net.forward(outputLayers)
    t, _ = net.getPerfProfile()

    # Get scores and geometry
    scores = output[0]
    geometry = output[1]
    [boxes, confidences] = decode(scores, geometry, confThreshold)
    # Apply NMS
    global indices
    indices = cv.dnn.NMSBoxesRotated(boxes, confidences, confThreshold,nmsThreshold)

    all_angles = []
    for i in indices:
        vertices = cv.boxPoints(boxes[i[0]])
        angles = []
        for j in range(4):
            vertices[j][0] *= rW
            vertices[j][1] *= rH
        for j in range(4):
            p1 = (vertices[j][0], vertices[j][1])
            p2 = (vertices[(j + 1) % 4][0], vertices[(j + 1) % 4][1])
            rad = math.atan2(p2[0]-p1[0], p2[1]-p1[1])
            ang = math.degrees(rad)
            angles.append(ang)
        all_angles.append(angles)
    rots = [i[3] for i in all_angles]
    avg_rot = sum(rots)/len(rots)
    global sufix
    frame, sufix = correct(frame, avg_rot)
    # print('lecture', IN)
    cv.imwrite( '/temp/im.jpg', frame)
    im = '/temp/im.jpg'
    if 'bnw' in sufix:
        h, w = frame.shape
    else:
        h, w, _ = frame.shape

    with io.open(im, 'rb') as image_file:
                content = image_file.read()

    image = vision.Image(content=content)
    global response
    response = client.text_detection(image=image)
    data = []
    for text in response.text_annotations:
        # prin
        vertices = ['(%s,%s)' % (v.x, v.y) for v in text.bounding_poly.vertices]
        xs = [v.x for v in text.bounding_poly.vertices]
        ys = [v.y for v in text.bounding_poly.vertices]
        data.append([text.description, xs[0], ys[0], xs[1], ys[1],xs[2], ys[2], xs[3], ys[3]])
    return data

def extract(input_list):
    campos_=[]
    for i in range(len(input_list)):
        for g in campos:
            A = []
            # print('comparacion_______',g, input_list[i])
            comparacion = fuzz.ratio(g, input_list[i][0].lower())
            if comparacion > 83:
                # print(comparacion, g, input_list[i][0].lower())
                A.append(i)
                A.append(g)
                A.extend(input_list[i])
                campos_.append(A)
    # print('jkna,ksjhbf',campos_)
    global cifras_
    cifras_ = []
    for i in range(len(input_list)):
        if cifra.match(input_list[i][0]):
            # print(input_list[i])
            A = []
            A.append(i)
            A.extend(input_list[i])
            cifras_.append(A)
    cifras_ = [i for i in cifras_ if '$' in i[1]]
    global cifras_candidatos
    cifras_candidatos = []
    response = {'code': 200,
                'data': [{
                    'balance general': {'caja y efectivo': '',
                        'total activo': '',
                        'total pasivo': '',
                            'total patrimonio': ''},
                    'estados perdidas y ganancias': {'costo de ventas': '',
                        'utilidad antes de impuestos': '',
                        'utilidad bruta': '',
                        'utilidad neta': '',
                        'utilidad operacional': '',
                        'ventas': ''},
                    'general': {'fecha': '', 'unidades de medida': ''}}],
                'message': 'success'}
    #response['data'][0]['balance general']['caja y efectivo']
    for i in range(len(campos_)):
        print('field',campos_)
        x1 = campos_[i][9]
        y1 = campos_[i][10]
        x2 = campos_[i][7]
        y2 = campos_[i][8]
        A = []
        for g in range(len(cifras_)):
            x_2 = cifras_[g][5]
            y_2 = cifras_[g][6]
            rad = math.atan2(y_2-y1, x_2-x1)
            ang = math.degrees(rad)
            B = []
            B.append(cifras_[g])
            B.extend([ang, math.sqrt((x_2 - x2)**2 + (y_2 - y2)**2)])
            A.append(B)
            # print('aaaaaaaaaa',A)
            # A = [o for o in A if o[-2] > -1.9 and 91 > o[-2]]
            A = sorted(A, key=lambda tup: tup[-1], reverse = False)
        # print('###',i,campos_[i])
        # print('AAAA##################',A)
        if A != []:
            cifras_candidatos.append(A[0])
    # print
    # print('check', [i for i in campos_ if i[1] == 'activo'])
    try:
        activo = campos_.index([i for i in campos_ if i[1] == 'activo'][-1])
        activo = cifras_candidatos[activo][0][1:]
    except Exception as e:
        activo = ''
        print(e)
    try:
        pasivo = campos_.index([i for i in campos_ if i[1] == 'pasivo'][-1])
        pasivo = cifras_candidatos[pasivo][0][1:]
    except Exception as e:
        pasivo =  ''
        print(e)
    try:
        patrimonio = campos_.index([i for i in campos_ if i[1] == 'patrimonio'][-1])
        patrimonio = cifras_candidatos[patrimonio][0][1:]
    except Exception as e:
        patrimonio = ''
        print(e)
    try:
        efectivo = campos_.index([i for i in campos_ if i[1] == 'efectivo'][-1])
        efectivo = cifras_candidatos[efectivo][0][1:]
    except Exception as e:
        efectivo = ''
        print(e)

    response['data'][0]['balance general']['caja y efectivo'] = efectivo
    response['data'][0]['balance general']['total activo'] = activo
    response['data'][0]['balance general']['total pasivo'] = pasivo
    response['data'][0]['balance general']['total patrimonio'] = patrimonio
    # act
    # return campos_, cifras_candidatos
    return response

@app.route('/extract', methods = ['POST'])
def get_all():
    cv.destroyAllWindows()
    global IN
    IN = request.files['IN'].read()
    name = request.files['IN'].name
    # npimg = np.fromstring(IN, np.uint8)
    # img = cv.imdecode(npimg, cv.IMREAD_COLOR)
    global imgs
    imgs = []
    doc = fitz.open('pdf',IN)
    for i in range(len(doc)):
        for img in doc.getPageImageList(i):
            xref = img[0]
            pix = fitz.Pixmap(doc, xref)
            if pix.n < 5:       # this is GRAY or RGB
                pix.writePNG("temp/p%s-%s.png" % (i, xref))
                imgs.append("temp/p%s-%s.png" % (i, xref))
            else:               # CMYK: convert to RGB first
                pix1 = fitz.Pixmap(fitz.csRGB, pix)
                pix1.writePNG("temp/p%s-%s.png" % (i, xref))
                imgs.append("temp/p%s-%s.png" % (i, xref))
                pix1 = None
            # print(pix)
            pix = None
    # Im = cv.imread(IN)
    global data, result
    data = []
    images = []
    for img in imgs:
        image = cv.imread(img)
        images.append(image)
        data.append(get_lecture(image))
    # global cp, extracted
    # cp, extracted = extract(data[1:])
    response = extract(data[1:])
    # result = {'lectura':data[1:], respo}
    cv.destroyAllWindows()
    # values = get_all_values(data)
    # return values
    # return result
    response['name'] = name
    response['data'][0]['imagen'] = images[0].tolist()
    return response
#%%
if __name__ == "__main__":
    cv.destroyAllWindows()
    aaa = time.time()
    # Read and store arguments
    # IN = 'EstadodeSituacionFinancieraal31deMarzode2015_page-0001.jpg'
    width = 320
    height = 320
    thr = 0.5
    nms = 0.4

    confThreshold = thr
    nmsThreshold = nms
    inpWidth = width
    inpHeight = height
    # model = args.model
    model = "frozen_east_text_detection.pb-master/frozen_east_text_detection.pb"

    # cifra = re.compile('|'.join([
    #   '^-?\$?(\-\d*\.\d{1,2})$'
    #   '^-?\$?(\d*\,\d{1,3})$',
    #   '^-?\$?(\d*\,\d{1,3},\d{1,3})$',
    #   '^-?\?(\d*\,\d{1,3},\d{1,3})$',
    #   '^-?\$?(\d*\,\d{1,3}\.\d{1,2})$',
    #   '^-?\$?(\d*\.\d{1,3}\.\d{1,2})$',
    #   '^-?\$?(\d*\.\d{1,2})$',
    #   '^-?\$?(\-\d*\,\d{1,3})$',
    #   '^-?\$?(\-\d*\,\d{1,3}\.\d{1,2})$',
    #   '^-?\$?(\-\d*\.\d{1,3}\.\d{1,2})$',
    #   '^-?\$?(\-\d*\.\d{1,2})$',
    #   '^\$0$',
    # ]))

    cifra = re.compile('^-?\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$')

    fecha = re.compile('|'.join([
      '^([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))(\/)\d{4}$',
      '^([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))(\/)\d{4}T$',
      '^([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))(\/)\d{4}T([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',
      '^([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))(\/)\d{4} ([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',

      '^\d{4}(\/)([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))$',
      '^\d{4}(\/)([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))T$',
      '^\d{4}(\/)([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))T([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',
      '^\d{4}(\/)([0-2][0-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2])) ([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',

      '^\d{4}(\/)(((0)[1-9])|((1)[0-2]))(\/)([0-2][0-9]|(3)[0-1])$',
      '^\d{4}(\/)(((0)[1-9])|((1)[0-2]))(\/)([0-2][0-9]|(3)[0-1])T$',
      '^\d{4}(\/)(((0)[1-9])|((1)[0-2]))(\/)([0-2][0-9]|(3)[0-1])T([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',
      '^\d{4}(\/)(((0)[1-9])|((1)[0-2]))(\/)([0-2][0-9]|(3)[0-1]) ([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',

      '^([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2]))(\-)\d{4}$',
      '^([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2]))(\-)\d{4}T$',
      '^([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2]))(\-)\d{4}T([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',
      '^([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2]))(\-)\d{4} ([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',

      '^\d{4}(\-)([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2]))$',
      '^\d{4}(\-)([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2]))T$',
      '^\d{4}(\-)([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2]))T([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',
      '^\d{4}(\-)([0-2][0-9]|(3)[0-1])(\-)(((0)[1-9])|((1)[0-2])) ([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',

      '^\d{4}(\-)(((0)[1-9])|((1)[0-2]))(\-)([0-2][0-9]|(3)[0-1])$',
      '^\d{4}(\-)(((0)[1-9])|((1)[0-2]))(\-)([0-2][0-9]|(3)[0-1])T$',
      '^\d{4}(\-)(((0)[1-9])|((1)[0-2]))(\-)([0-2][0-9]|(3)[0-1])T([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',
      '^\d{4}(\-)(((0)[1-9])|((1)[0-2]))(\-)([0-2][0-9]|(3)[0-1]) ([0-1][0-9]|(2)[0-3])(\:)([0-5][0-9])(\:)([0-5][0-9])$',

      # '^([0-2][0-9]|(3)[0-1])(\de)(((0)[1-9])|((1)[0-2]))(\/)\d{4}$',

    ]))

    rfc = re.compile('|'.join([
      '^[0-9a-fA-z]{3}\d{6}[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{3} \d{6} [0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{3}\d{6} [0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{3} \d{6}[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{3}-\d{6}-[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{3}-\d{6}[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{4}\d{6}[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{4} \d{6} [0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{4}\d{6} [0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{4} \d{6}[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{4}-\d{6}-[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{4}-\d{6}[0-9a-fA-z]{3}$',
      '^[0-9a-fA-z]{4}-\d{6}$',
      '^[0-9a-fA-z]{4} \d{6}$',
      '^[0-9a-fA-z]{4}\d{6}$',
    ]))
    # campos = [
    #     'caja y efectivo',
    #     'total activo',
    #     'total pasivo',
    #     'total patrimonio',
    #     'costo de ventas',
    #     'utilidad antes de impuestos',
    #     'utilidad bruta',
    #     'utilidad neta',
    #     'utilidad operacional',
    #     'ventas',
    #     'fecha',
    #     'unidades de medida'
    #     ]
    campos = [
        'efectivo',
        'activo',
        'pasivo',
        'patrimonio',
        'impuestos',
        'bruta',
        'neta',
        'operacional',
        'ventas',
        'fecha',
        'medida'
        ]

    net = cv.dnn.readNet(model)
    cv.destroyAllWindows()
    cv.waitKey(0)
    app.run()
