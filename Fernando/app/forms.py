from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, EqualTo
from flask_wtf.file import FileField, FileAllowed, FileRequired


class UploadForm(FlaskForm):
    file = FileField('Archivo', validators=[FileAllowed(['pdf', 'jpg', 'png', 'jpeg']), FileRequired()])
    submit = SubmitField('Analizar')
