
if __name__ == '__main__':
    pass
#!/usr/bin/env python3

# from app import app
from operator import itemgetter
from itertools import chain
import io
import cv2 as cv
import os
import math
import imutils
import numpy as np
# import pytesseract
import pandas as pd
import fitz
import io
from pdf2image import convert_from_path
# import imageio
import re
from fuzzywuzzy import fuzz
import time
import unicodedata
import sys
# sys.path.append('/home/dario/PycharmProjects/hackaton2020 (3rd copy)/OCR')
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
# from flask import jsonify
# from flask import Flask, request
import config
from google.cloud import vision
# os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.getcwd() + '/MyProject24836-9cefd1a1f9ec.json'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config.GOOGLE_CREDS
client = vision.ImageAnnotatorClient()

# app = Flask(__name__)
# %%
'''
este método sirve para detectar cuadros de texto y calcular la inclinacion del
documento
'''
def decode(scores, geometry, scoreThresh):
    detections = []
    confidences = []

    ############ CHECK DIMENSIONS AND SHAPES OF geometry AND scores ############
    assert len(scores.shape) == 4, "Incorrect dimensions of scores"
    assert len(geometry.shape) == 4, "Incorrect dimensions of geometry"
    assert scores.shape[0] == 1, "Invalid dimensions of scores"
    assert geometry.shape[0] == 1, "Invalid dimensions of geometry"
    assert scores.shape[1] == 1, "Invalid dimensions of scores"
    assert geometry.shape[1] == 5, "Invalid dimensions of geometry"
    assert scores.shape[2] == geometry.shape[2], "Invalid dimensions of scores and geometry"
    assert scores.shape[3] == geometry.shape[3], "Invalid dimensions of scores and geometry"
    height = scores.shape[2]
    width = scores.shape[3]
    for y in range(0, height):

        # Extract data from scores
        scoresData = scores[0][0][y]
        x0_data = geometry[0][0][y]
        x1_data = geometry[0][1][y]
        x2_data = geometry[0][2][y]
        x3_data = geometry[0][3][y]
        anglesData = geometry[0][4][y]
        for x in range(0, width):
            score = scoresData[x]

            # If score is lower than threshold score, move to next x
            if (score < scoreThresh):
                continue

            # Calculate offset
            offsetX = x * 4.0
            offsetY = y * 4.0
            angle = anglesData[x]

            # Calculate cos and sin of angle
            cosA = math.cos(angle)
            sinA = math.sin(angle)
            h = x0_data[x] + x2_data[x]
            w = x1_data[x] + x3_data[x]

            # Calculate offset
            offset = (
            [offsetX + cosA * x1_data[x] + sinA * x2_data[x], offsetY - sinA * x1_data[x] + cosA * x2_data[x]])

            # Find points for rectangle
            p1 = (-sinA * h + offset[0], -cosA * h + offset[1])
            p3 = (-cosA * w + offset[0], sinA * w + offset[1])
            center = (0.5 * (p1[0] + p3[0]), 0.5 * (p1[1] + p3[1]))
            detections.append((center, (w, h), -1 * angle * 180.0 / math.pi))
            confidences.append(float(score))

    # Return detections and confidences
    return [detections, confidences]


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii.decode("utf-8")


def correct(frame, avg_rot, rotate=False):
    if rotate == True:
        frame = imutils.rotate_bound(frame, avg_rot + 90)
    return frame

def get_lecture(IN, just_lecture):
    global imgs
    imgs = []
    doc = fitz.open(IN)
    '''
    aqui se convierten a imagenes las paginas del pdf y se
    guardan en un directorio temporal
    '''
    for i in range(len(doc)):
        for img in doc.getPageImageList(i):
            xref = img[0]
            pix = fitz.Pixmap(doc, xref)
            if pix.n < 5:  # this is GRAY or RGB
                pix.writePNG(config.TMP+"p%s-%s.png" % (i, xref))
                imgs.append(config.TMP+"p%s-%s.png" % (i, xref))
            else:  # CMYK: convert to RGB first
                pix1 = fitz.Pixmap(fitz.csRGB, pix)
                pix1.writePNG(config.TMP+"p%s-%s.png" % (i, xref))
                imgs.append(config.TMP+"p%s-%s.png" % (i, xref))
                pix1 = None
            # print(pix)
            pix = None
    '''
    se crea una lista de imágenes leídas, son las páginas del pdf
    '''
    images = []
    for img in imgs:
        image = cv.imread(img)
        images.append(image)
    global Data
    Data = []
    
    for r in range(len(images)):
        frame = images[r]
        outputLayers = []
        outputLayers.append("feature_fusion/Conv_7/Sigmoid")
        outputLayers.append("feature_fusion/concat_3")
        height_ = frame.shape[0]
        width_ = frame.shape[1]
        rW = width_ / float(inpWidth)
        rH = height_ / float(inpHeight)
    
        blob = cv.dnn.blobFromImage(frame, 1.0, (inpWidth, inpHeight), (123.68, 116.78, 103.94), True, False)
    
        net.setInput(blob)
        output = net.forward(outputLayers)
        t, _ = net.getPerfProfile()
    
        # Get scores and geometry
        scores = output[0]
        geometry = output[1]
        [boxes, confidences] = decode(scores, geometry, confThreshold)
        # Apply NMS
        global indices
        indices = cv.dnn.NMSBoxesRotated(boxes, confidences, confThreshold, nmsThreshold)
    
        all_angles = []
        for i in indices:
            vertices = cv.boxPoints(boxes[i[0]])
            angles = []
            for j in range(4):
                vertices[j][0] *= rW
                vertices[j][1] *= rH
            for j in range(4):
                p1 = (vertices[j][0], vertices[j][1])
                p2 = (vertices[(j + 1) % 4][0], vertices[(j + 1) % 4][1])
                rad = math.atan2(p2[0] - p1[0], p2[1] - p1[1])
                ang = math.degrees(rad)
                angles.append(ang)
            all_angles.append(angles)
        rots = [i[3] for i in all_angles]
        try:
            avg_rot = sum(rots) / len(rots)
            frame= correct(frame, avg_rot)
            cv.imwrite(config.TMP+'im.jpg', frame)
            im = config.TMP+'im.jpg'
            h, w, _ = frame.shape
        
            with io.open(im, 'rb') as image_file:
                content = image_file.read()
        except Exception:
            cv.imwrite(config.TMP+'im.jpg', frame)
            im = config.TMP+'im.jpg'
            h, w, _ = frame.shape
        
            with io.open(im, 'rb') as image_file:
                content = image_file.read()
        image = vision.Image(content=content)
        global response
        
        '''
        llamada a api de google cloud vision
        '''
        response = client.text_detection(image=image)
        data = []
        for text in response.text_annotations:
            # prin
            vertices = ['(%s,%s)' % (v.x, v.y) for v in text.bounding_poly.vertices]
            xs = [v.x for v in text.bounding_poly.vertices]
            ys = [v.y for v in text.bounding_poly.vertices]
            data.append([text.description, xs[0], ys[0], xs[1], ys[1], xs[2], ys[2], xs[3], ys[3]])
        data = sorted(data, key=itemgetter(2, 0))
        '''
        si se requiere solo la lectura se entrega una lectura global
        si se requiere extraer los datos se entrega una lista de lecturas de las páginas
        '''
        if just_lecture == True:
            Data.extend(data[1:])
        elif just_lecture == False:
            Data.append(data[1:])
    return Data

def extr_text(nombre):
  PATH = nombre
  pages = convert_from_path(PATH, 501)
  listaResponse = ""
  client = vision.ImageAnnotatorClient()

  for page in pages:
    page.save('tmp.jpg')
    with io.open('tmp.jpg', 'rb') as image_file:
        content = image_file.read()
    os.system('rm tmp.jpg')
    image = vision.Image(content=content)
    response = client.text_detection(image=image)
    try:
      listaResponse += response.text_annotations[0].description + " "
    except:
      listaResponse +=""

  textoResponse = listaResponse.replace("\n", " ")
  return textoResponse.split()


# %%
def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                    if unicodedata.category(c) != 'Mn')

'''
el siguiente método separa la lectura de las páginas en renglones
el y_jump_index define la relación entre alturas de palabras para
determinar si se salta o no renglón
'''
def split_list(lectura_sorted, y_jump_index):
    
    # print('#######lectura_sorted',lectura_sorted)
    # print('#######yjump',y_jump_index)
    for r in range(len(lectura_sorted)):
        lectura_sorted[r].extend(
            [lectura_sorted[r][3] - lectura_sorted[r][1], lectura_sorted[r][6] - lectura_sorted[r][4]])
    for r in range(len(lectura_sorted)):
        if r > 0:
            lectura_sorted[r].append(lectura_sorted[r][8] - lectura_sorted[r - 1][8])
        else:
            lectura_sorted[r].append(0)
    for r in range(len(lectura_sorted)):
        if r > 0:
            # print('#######,amoave', lectura_sorted[r][11], lectura_sorted[r - 1][10])    
            try:
                if lectura_sorted[r][11] / lectura_sorted[r - 1][10] >= y_jump_index:
                    lectura_sorted[r].append('jump')
                else:
                    lectura_sorted[r].append('')
            except Exception:
                lectura_sorted[r].append('')
                # print(e)
        else:
            lectura_sorted[r].append('')
           
    jumps = [lectura_sorted.index(i) for i in lectura_sorted if i[-1] == 'jump']
    temp = zip(chain([0], jumps), chain(jumps, [None]))
    global res
    res = [lectura_sorted[i:j] for i, j in temp]
    for r in range(len(res)):
        for q in range(len(res[r])):
            res[r][q] = res[r][q][:9]
    for i in range(len(res)):
        res[i] = sorted(res[i], key=itemgetter(1, 0))
    return res

'''
este método encuentra cifras monetarias en la página leída
y se separa la lectura como en el ejemplo general siguiente:
    [activo total $40,000.00 efectivo -$333.00]
    [activo, total] [efectivo] [$40,000.00, -$333.00]
'''
def find_ammounts(palabras):
    cifras_ = []
    palabras = [i[:9] for i in palabras]
    for r in range(len(palabras)):
        if cifra.match(palabras[r][0]) and not any_d.match(palabras[r][0]):
            palabras[r].append('cut')
            cifras_.append(palabras[r])
        else:
            continue
    for r in range(len(palabras)):
        if r > 0:
            if palabras[r - 1][9] == 'cut':
                palabras[r].append('jump')
            else:
                palabras[r].append('')
        else:
            palabras[r].append('')
    palabras = [i for i in palabras if i[9] != 'cut']
    jumps = [palabras.index(i) for i in palabras if i[-1] == 'jump']
    temp = zip(chain([0], jumps), chain(jumps, [None]))
    palabras = [palabras[i:j] for i, j in temp]
    return palabras, cifras_

'''
el siguiente método toma la lista de listas de palabras creadas con 
el método find_ammounts y las une como en el ejemplo general siguiente:
    [activo, total] [efectivo]
    [activo total] [efectivo]
esto nos servirá para encontrar los campos deseados por medio de una comparación difusa 
con el diccionario de campos
'''
def get_cifras_palabras(splat_list):
    # for i in range(len(D)):
    #     D[i] = D[i][:9]
    global full_palabras
    full_palabras = []
    full_cifras = []
    
    for r in range(len(splat_list)):
        A, B = find_ammounts(splat_list[r])
        full_palabras.append(A)
        full_cifras.append(B)
    # print('000',A)
    joint_palabras = []
    
    for i in full_palabras:
        for j in i:
            if j != []:
                # print('###',[D.index(u[:9]) for u in j])
                A = [' '.join([u[0] for u in j])]
                A.extend(j[0][1:3])
                A.extend(j[-1][3:5])
                A.extend(j[-1][5:7])
                A.extend(j[0][7:9])
                # A.append([D.index(u[:9]) for u in j])
                # A.append()
                joint_palabras.append(A)
            else:
                continue
    global disjointed_cifras
    disjointed_cifras = []
    for i in full_cifras:
        if i != []:
            disjointed_cifras.extend(i)
    # for i in range(len(disjointed_cifras)):
    #     disjointed_cifras[i].append(D.index(disjointed_cifras[i][:9]))
        # print(D.index(disjointed_cifras[i][:9]))
    return joint_palabras, disjointed_cifras

'''
el próximo método itera entre los resultados del metodo anterior y los compara
iterativamente con las diferentes posibles variaciones en el nombrede los campos 
que se pueden encontrar en el diccionario de campos
'''
def get_campos(joint_palabras):
    global campos_
    campos_ = []
    for i in range(len(joint_palabras)):
        for c in campos.keys():
            for cc in campos[c]:
                if fuzz.ratio(cc, ''.join([i for i in strip_accents(joint_palabras[i][0]).lower() if not i.isdigit()])) >= 81:
                    campos_.append(
                        [fuzz.ratio(cc, strip_accents(joint_palabras[i][0]).lower()), c, cc, joint_palabras[i]])

    # for i in range(len(joint_palabras)):
    global pre_res
    pre_res = {}
    for c in campos.keys():
        try:
            A = [i for i in campos_ if i[1] == c]
            B = sorted(A, key=itemgetter(0, 0))[-1]
            # print(c,A,B)
            pre_res[c] = B
        except Exception:
            continue
    return pre_res

'''
en este punto se proponen los valores para los campos encontrados
'''
def pre_campo_valor(pre_res, disjointed_cifras):
    res = []
    for c in pre_res.keys():
        ccc = []
        # print('####', pre_res[c])
        for i in range(len(disjointed_cifras)):
            dist = math.sqrt((pre_res[c][-1][5] - disjointed_cifras[i][7]) ** 2 + (pre_res[c][-1][6] - disjointed_cifras[i][8]) ** 2)
            # rad = math.atan2(palabras[p][8]-palabras[p-1][6],palabras[p][7]-palabras[p-1][5])
            rad = math.atan2(disjointed_cifras[i][2] - pre_res[c][-1][2], disjointed_cifras[i][1] - pre_res[c][-1][1])
            ang = math.degrees(rad)
            ccc.append([c, ang, dist, disjointed_cifras[i]])
            ccc = [i for i in ccc if i[1] > -1 and 90 > i[1]]
            ccc = sorted(ccc, key=itemgetter(2, 0))
        # print('ccc',ccc)
        try:
            res.append([ccc[0][0], ccc[0][3][:9]])
        except Exception:
            continue
    return res


# %%
'''
el siguiente método integra al resto de los métodos posteriores para obtener los
pares campo-valor
'''
def get_all(IN):
    response = {'code': 200,
                'data': [{
                    'balance general': {'caja y efectivo': '',
                                        'total activo': '',
                                        'total pasivo': '',
                                        'total patrimonio': ''},
                    'estados perdidas y ganancias': {'costo de ventas': '',
                                                     'utilidad antes de impuestos': '',
                                                     'utilidad bruta': '',
                                                     'utilidad neta': '',
                                                     'utilidad operacional': '',
                                                     'ventas': ''},
                    'general': {'fecha': '', 'unidades de medida': ''}}],
                'message': 'success'}
    global splat_list,joint_palabras, pre_res, data
    
    cv.destroyAllWindows()
    
    data =get_lecture(IN, False)
    # data = data[1:]
    # data = sorted(data, key=itemgetter(2, 0))
    global full_splat_list, full_joint_palabras, full_disjointed_cifras, full_pre_res, full_res
    full_splat_list = []
    full_joint_palabras = []
    full_disjointed_cifras = []
    full_pre_res = []
    full_res = []
    for D in data:
        splat_list = split_list(D, y_jump_index)
        joint_palabras, disjointed_cifras = get_cifras_palabras(splat_list)
        pre_res = get_campos(joint_palabras)
        res = pre_campo_valor(pre_res, disjointed_cifras)
        
        full_splat_list.append(splat_list)
        # full_joint_palabras.append(joint_palabras)
        # full_disjointed_cifras.append(disjointed_cifras)
        # full_pre_res.append(pre_res)
        # full_res.extend(res)
    
    final_list = []
    for i in range(len(full_splat_list)):
        C = []
        for j in range(len(full_splat_list[i])):
            A = []
            B = []
            # print(i,j,full_splat_list[i][j])
            for k in range(len(full_splat_list[i][j])):
                full_splat_list[i][j][k].append(k)
                if cifra.match(full_splat_list[i][j][k][0]) and not any_d.match(full_splat_list[i][j][k][0]) :
                    full_splat_list[i][j][k].append('cut')
                    B.append(full_splat_list[i][j][k])
                    # print(full_splat_list[i][j][k])
                else:
                    full_splat_list[i][j][k].append('')
                    A.append(full_splat_list[i][j][k])
            C.append([A,B])
            # print('###')
            # print('AAA',A)
            # print('BBB',B)
        final_list.append(C)
    #%%
    final = []
    for i in full_splat_list:
        final.extend(i)
    #%%
    final_final = [ ]
    for t in range(len(final)):
        cuts = [final[t].index(i) for i in final[t] if i[-1] == 'cut']
        temp = zip(chain([0], cuts), chain(cuts, [None]))
        # global res
        ddd = [final[t][i:j] for i, j in temp]
        final_final.append(ddd)
        # print('####')
        # print(ddd)
    
    #%%
    global pre
    pre_ = []
    for i in final_final:
        C = []
        D = []
        # print('#-#-#---------------------------------------------------')
        for g in i:
            # print(g)
            # print('////')
            A = [' '.join([d[0] for d in g if d[-1] != 'cut'])]
            A.append([d[1:10] for d in g if d[-1] != 'cut'])
            C.append(A)
            B = [d for d in g if d[-1] == 'cut']
            D.append(B)
            # print(A)
        pre_.append([C,D])
        
    #%%
    campos_ = []
    # for h in pre_:
    #     for w in h:
    #         print(w)
            # for c in campos.keys():
            #         for cc in campos[c]:
            #             if fuzz.ratio(cc, ''.join([i for i in strip_accents(w[0]).lower() if not i.isdigit()])) >= 81:
            #                 campos_.append(
            #                     [fuzz.ratio(cc, strip_accents(w[0]).lower()), c, cc, w])
    
    response = {'code': 200,
                    'data': [{
                        'balance general': {'caja y efectivo': '',
                                            'total activo': '',
                                            'total pasivo': '',
                                            'total patrimonio': ''},
                        'estados perdidas y ganancias': {'costo de ventas': '',
                                                         'utilidad antes de impuestos': '',
                                                         'utilidad bruta': '',
                                                         'utilidad neta': '',
                                                         'utilidad operacional': '',
                                                         'ventas': ''},
                        'general': {'fecha': '', 'unidades de medida': ''}}],
                    'message': 'success'}
    
    for r in range(len(pre_)):
        for s in range(len(pre_[r][0])):
            for c in campos.keys():
                    for cc in campos[c]:
                        # print(pre_[r][0][s][0])
                        if fuzz.ratio(cc, ''.join([i for i in strip_accents(pre_[r][0][s][0]).lower() if not i.isdigit()])) >= 87:
                            campos_.append([r, pre_[r][0][s][-1][-1][-1] ,fuzz.ratio(cc, ''.join([i for i in strip_accents(pre_[r][0][s][0]).lower() if not i.isdigit()])), c, cc, pre_[r][0][s]])
    
    # global pre_res
    pre_res = {}
    for c in campos.keys():
        try:
            A = [i for i in campos_ if i[3] == c]
            B = sorted(A, key=itemgetter(2, 0), reverse = True)[0]
            # print(c,A,B)
            pre_res[c] = B
        except Exception:
            continue
    res = []
    X = []
    for c in pre_res.keys():
        try:
            val = sorted([ i[0] for i in pre_[pre_res[c][0]][1] if i != []], key=itemgetter(9, 0))[0][:9]
            # print
            if cifra.match(val[0]):
                pre_res[c].append(val)
                print(pre_res[c][-1])
            else:
                print(c)
                del pre_res[c]
            print(pre_res[c][-1])
        except Exception as e:
            print(c)
            X.append(c)
            # del pre_res[c]
            print(e)
            continue
    # for i in X:
    #     del pre_res(i)
    # return pre_res            
    #%%
    try:
        response['data'][0]['balance general']['caja y efectivo'] = pre_res["Caja y bancos"][6]
    except Exception:
        response['data'][0]['balance general']['caja y efectivo'] = ''
    try:
        response['data'][0]['balance general']['total activo'] = pre_res["Total activo"][6]
    except Exception:
        response['data'][0]['balance general']['total activo'] = ''
    try:
        response['data'][0]['balance general']['total pasivo'] = pre_res["Total pasivo"][6]
    except Exception:
        response['data'][0]['balance general']['total pasivo'] =''
    try:    
        response['data'][0]['balance general']['total patrimonio'] = pre_res["Total patrimonio"][6]
    except Exception:
        response['data'][0]['balance general']['total patrimonio'] =''
    try:
        response['data'][0]['estados perdidas y ganancias']['costo de ventas'] = pre_res["Costo de ventas"][6]
    except Exception:
        response['data'][0]['estados perdidas y ganancias']['costo de ventas'] = ''
    try:
        response['data'][0]['estados perdidas y ganancias']['utilidad antes de impuestos'] = pre_res["Utilidad antes de impuestos"][6]
    except Exception:
        response['data'][0]['estados perdidas y ganancias']['utilidad antes de impuestos'] = ''
    try:
        response['data'][0]['estados perdidas y ganancias']['utilidad bruta'] = pre_res["Utilidad Bruta"][6]
    except Exception:  
        response['data'][0]['estados perdidas y ganancias']['utilidad bruta'] = ''
    try:
        response['data'][0]['estados perdidas y ganancias']['utilidad neta'] = pre_res["Utilidad neta"][6]
    except Exception:  
        response['data'][0]['estados perdidas y ganancias']['utilidad neta'] =''
    try:
        response['data'][0]['estados perdidas y ganancias']['utilidad operacional'] = pre_res["Utilidad operacional"][6]
    except Exception:    
        response['data'][0]['estados perdidas y ganancias']['utilidad operacional'] =''
    try:
        response['data'][0]['estados perdidas y ganancias']['ventas'] = pre_res["Ventas"][6]
    except Exception:   
        response['data'][0]['estados perdidas y ganancias']['ventas'] =''
        
    response['data'][0]['general']['fecha'] = ''
    
    response['data'][0]['general']['unidades de medida'] = ''
    
    
    
    
    # print('####',res)
    cv.destroyAllWindows()

    response['name'] = IN
    return response


# %%
def initOCR():
    cv.destroyAllWindows()
    # aaa = time.time()
    # Read and store arguments
    # IN = 'EstadodeSituacionFinancieraal31deMarzode2015_page-0001.jpg'
    width = 320
    height = 320
    thr = 0.5
    nms = 0.4
    global y_jump_index
    y_jump_index = 0.9
    global confThreshold, nmsThreshold, inpWidth, inpHeight

    confThreshold = thr
    nmsThreshold = nms
    inpWidth = width
    inpHeight = height
    # model = args.model
    model = config.FROZEN_EAST

    global cifra, any_d
    
    
    cifra = re.compile('|'.join([
        '^-?\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$',
        '^-?\$?([0-9]{1,3}.([0-9]{3}.)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$',
        '^\(([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])\)?',
        '^\(([0-9]{1,3}.([0-9]{3}.)*[0-9]{3}|[0-9]+)(.[0-9][0-9])\)?',
        ]))
    any_d = re.compile('^\d{1,1000000}$')
    global campos
    # campos = {
    #     "Caja y bancos": ["caja y bancos", "efectivo y equivalentes en efectivo", "efectivo y equivalentes", "efectivo y equivalentes de efectivo"],
    #     "Total activo": ["total activo", "suma de los activos ", "activo total"],
    #     "Total pasivo": ["total pasivo", "suma de los pasivos", "pasivo total"],
    #     "Total patrimonio": ["total patrimonio", "suma de los patrimonios", "patrimonio total", "patrimonio",
    #                           "suma el capital contable", "total capital contable"],
    #     "Ventas": ["ventas", "ventas por operación ordinaria", "ingresos por operación",
    #                 "ingresos por operación ordinaria", "ingresos operacionales", "ventas brutas",
    #                 "ingreso por actividades ordinarias"],
    #     "Costo de ventas": ["costo de ventas", "costos por ventas", "costo de actividades ordinarias","costos de ventas"],
    #     "Utilidad Bruta": ["utilidad bruta", "perdida bruta"],
    #     "Utilidad operacional": ["utilidad operacional", "perdida operacional"],
    #     "Utilidad antes de impuestos": ["utilidad antes de impuestos", "perdida antes de impuestos"],
    #     "Utilidad neta": ["utilidad neta", "perdida neta", "resultado neto", "resultado neto del ano"]
    # }

    # stopwords 
    campos = {
        "Caja y bancos": ["caja y bancos", "efectivo y equivalentes en efectivo", "efectivo y equivalentes", "efectivo y equivalentes de efectivo"],
        "Total activo": ["total activo", "suma de los activos ", "activo total", "total del activo"],
        "Total pasivo": ["total pasivo", "suma de los pasivos", "pasivo total", "total del pasivo"],
        "Total patrimonio": ["total patrimonio", "suma de los patrimonios", "patrimonio total", "patrimonio",
                              "suma el capital contable", "total capital contable"],
        "Ventas": ["ventas", "ventas por operación ordinaria", "ingresos por operación",
                    "ingresos por operación ordinaria", "ingresos operacionales", "ventas brutas",
                    "ingreso por actividades ordinarias", "ingreso por ventas"],
        "Costo de ventas": ["costo de ventas", "costos por ventas", "costo de actividades ordinarias","costos de ventas"],
        "Utilidad Bruta": ["utilidad bruta", "perdida bruta", "ganancia bruta", "ganancia bruta", "margen bruto", "beneficio bruto"],
        "Utilidad operacional": ["utilidad operacional", "perdida operacional", "utilidad operacion", "utilidad operativa", "resultado de la operacion", "ganancia por actividades de la operacion", "total ingresos operacionales", "ingresos operacionales", "resultado de actividades de la operacion"],
        "Utilidad antes de impuestos": ["utilidad antes de impuestos", "perdida antes de impuestos", "utilidad antes de impuesto sobre la renta", "utilidad antes de isr", "utilidad antes de impuestos sobre la ganancia", "ganancia perdida antes de impuestos", "utilidad antes de impuesto a las ganancias", "ganancia antes de impuesto a la renta", "utilidad antes de impuesto a las"],
        "Utilidad neta": ["utilidad neta", "perdida neta", "resultado neto", "resultado neto del ano", "utilidad neta del periodo", "ganancia neta del periodo","ganancia perdidas neta" , "utilidad neta consolidada", "utilidad neta del ejercicio"]
    }
    
    global net, ddd
    net = cv.dnn.readNet(model)
    # ddd = get_all('/home/dario/Downloads/Doc1.pdf')
    cv.destroyAllWindows()
    cv.waitKey(0)
    
if __name__ == '__main__':

    pass
    # app.run()
# initOCR()
# respondse = get_all('/home/dario/Downloads/Info para entregar/Doc1.pdf')
# lectura = get_lecture('//home/dario/Downloads/Info para entregar/Doc50.pdf')

# respondse = get_all('/home/dario/Downloads/Info para entregar/Doc4.pdf')
# lectura = get_lecture('//home/dario/Downloads/Info para entregar/Doc4.pdf')

# respondse = get_all('/home/dario/Downloads/prueba1.pdf')
# lectura = get_lecture('//home/dario/Downloads/prueba1.pdf')