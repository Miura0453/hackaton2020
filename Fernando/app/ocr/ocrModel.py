#!/usr/bin/env python3

from app import app
from operator import itemgetter
from itertools import chain
import io
import cv2 as cv
import os
import math
import imutils
import numpy as np
# import pytesseract
import pandas as pd
import fitz
import io
from pdf2image import convert_from_path
# import imageio
import re
from fuzzywuzzy import fuzz
import time
import unicodedata

from flask import jsonify
from flask import Flask, request

from google.cloud import vision

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'MyProject24836-9cefd1a1f9ec.json'
client = vision.ImageAnnotatorClient()


# %%

def decode(scores, geometry, scoreThresh):
    detections = []
    confidences = []

    ############ CHECK DIMENSIONS AND SHAPES OF geometry AND scores ############
    assert len(scores.shape) == 4, "Incorrect dimensions of scores"
    assert len(geometry.shape) == 4, "Incorrect dimensions of geometry"
    assert scores.shape[0] == 1, "Invalid dimensions of scores"
    assert geometry.shape[0] == 1, "Invalid dimensions of geometry"
    assert scores.shape[1] == 1, "Invalid dimensions of scores"
    assert geometry.shape[1] == 5, "Invalid dimensions of geometry"
    assert scores.shape[2] == geometry.shape[2], "Invalid dimensions of scores and geometry"
    assert scores.shape[3] == geometry.shape[3], "Invalid dimensions of scores and geometry"
    height = scores.shape[2]
    width = scores.shape[3]
    for y in range(0, height):

        # Extract data from scores
        scoresData = scores[0][0][y]
        x0_data = geometry[0][0][y]
        x1_data = geometry[0][1][y]
        x2_data = geometry[0][2][y]
        x3_data = geometry[0][3][y]
        anglesData = geometry[0][4][y]
        for x in range(0, width):
            score = scoresData[x]

            # If score is lower than threshold score, move to next x
            if (score < scoreThresh):
                continue

            # Calculate offset
            offsetX = x * 4.0
            offsetY = y * 4.0
            angle = anglesData[x]

            # Calculate cos and sin of angle
            cosA = math.cos(angle)
            sinA = math.sin(angle)
            h = x0_data[x] + x2_data[x]
            w = x1_data[x] + x3_data[x]

            # Calculate offset
            offset = (
            [offsetX + cosA * x1_data[x] + sinA * x2_data[x], offsetY - sinA * x1_data[x] + cosA * x2_data[x]])

            # Find points for rectangle
            p1 = (-sinA * h + offset[0], -cosA * h + offset[1])
            p3 = (-cosA * w + offset[0], sinA * w + offset[1])
            center = (0.5 * (p1[0] + p3[0]), 0.5 * (p1[1] + p3[1]))
            detections.append((center, (w, h), -1 * angle * 180.0 / math.pi))
            confidences.append(float(score))

    # Return detections and confidences
    return [detections, confidences]


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii.decode("utf-8")


def correct(frame, avg_rot, rotate=False):
    if rotate == True:
        frame = imutils.rotate_bound(frame, avg_rot + 90)
    return frame

def get_lecture(frame):
    global data
    data = []
    outputLayers = []
    outputLayers.append("feature_fusion/Conv_7/Sigmoid")
    outputLayers.append("feature_fusion/concat_3")
    height_ = frame.shape[0]
    width_ = frame.shape[1]
    rW = width_ / float(inpWidth)
    rH = height_ / float(inpHeight)

    blob = cv.dnn.blobFromImage(frame, 1.0, (inpWidth, inpHeight), (123.68, 116.78, 103.94), True, False)

    net.setInput(blob)
    output = net.forward(outputLayers)
    t, _ = net.getPerfProfile()

    # Get scores and geometry
    scores = output[0]
    geometry = output[1]
    [boxes, confidences] = decode(scores, geometry, confThreshold)
    # Apply NMS
    global indices
    indices = cv.dnn.NMSBoxesRotated(boxes, confidences, confThreshold, nmsThreshold)

    all_angles = []
    for i in indices:
        vertices = cv.boxPoints(boxes[i[0]])
        angles = []
        for j in range(4):
            vertices[j][0] *= rW
            vertices[j][1] *= rH
        for j in range(4):
            p1 = (vertices[j][0], vertices[j][1])
            p2 = (vertices[(j + 1) % 4][0], vertices[(j + 1) % 4][1])
            rad = math.atan2(p2[0] - p1[0], p2[1] - p1[1])
            ang = math.degrees(rad)
            angles.append(ang)
        all_angles.append(angles)
    rots = [i[3] for i in all_angles]
    ##########################################################
    try:
        avg_rot = sum(rots) / len(rots)
        frame= correct(frame, avg_rot)
        # print('lecture', IN)
        cv.imwrite('app/ocr/temp/im.jpg', frame)
        im = 'app/ocr/temp/im.jpg'
        # if 'bnw' in sufix:
        #     h, w = frame.shape
        # else:
        h, w, _ = frame.shape
    
        with io.open(im, 'rb') as image_file:
            content = image_file.read()
    except Exception as e:
        cv.imwrite('app/ocr/temp/im.jpg', frame)
        im = 'app/ocr/temp/im.jpg'
        # if 'bnw' in sufix:
        #     h, w = frame.shape
        # else:
        h, w, _ = frame.shape
    
        with io.open(im, 'rb') as image_file:
            content = image_file.read()
        print(e)
    ##############################################
    image = vision.Image(content=content)
    global response
    response = client.text_detection(image=image)
    data = []
    for text in response.text_annotations:
        # prin
        vertices = ['(%s,%s)' % (v.x, v.y) for v in text.bounding_poly.vertices]
        xs = [v.x for v in text.bounding_poly.vertices]
        ys = [v.y for v in text.bounding_poly.vertices]
        data.append([text.description, xs[0], ys[0], xs[1], ys[1], xs[2], ys[2], xs[3], ys[3]])
    # print('#################0',data)
    return jsonify(data)


# %%
def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


def split_list(lectura_sorted, y_jump_index):
    
    for r in range(len(lectura_sorted)):
        lectura_sorted[r].extend(
            [lectura_sorted[r][3] - lectura_sorted[r][1], lectura_sorted[r][6] - lectura_sorted[r][4]])
    for r in range(len(lectura_sorted)):
        if r > 0:
            lectura_sorted[r].append(lectura_sorted[r][8] - lectura_sorted[r - 1][8])
        else:
            lectura_sorted[r].append(0)
    for r in range(len(lectura_sorted)):
        if r > 0:
            try:
                if lectura_sorted[r][11] / lectura_sorted[r - 1][10] >= y_jump_index:
                    lectura_sorted[r].append('jump')
                else:
                    lectura_sorted[r].append('')
            except Exception as e:
                lectura_sorted[r].append('')
                print(e)
        else:
            lectura_sorted[r].append('')

    jumps = [lectura_sorted.index(i) for i in lectura_sorted if i[-1] == 'jump']
    temp = zip(chain([0], jumps), chain(jumps, [None]))
    res = [lectura_sorted[i:j] for i, j in temp]
    for r in range(len(res)):
        for q in range(len(res[r])):
            res[r][q] = res[r][q][:9]
    for i in range(len(res)):
        res[i] = sorted(res[i], key=itemgetter(1, 0))
    return res


def find_ammounts(palabras):
    cifras_ = []
    palabras = [i[:9] for i in palabras]
    for r in range(len(palabras)):
        if cifra.match(palabras[r][0]) and not any_d.match(palabras[r][0]):
            palabras[r].append('cut')
            cifras_.append(palabras[r])
        else:
            continue
    for r in range(len(palabras)):
        if r > 0:
            if palabras[r - 1][9] == 'cut':
                palabras[r].append('jump')
            else:
                palabras[r].append('')
        else:
            palabras[r].append('')
    palabras = [i for i in palabras if i[9] != 'cut']
    jumps = [palabras.index(i) for i in palabras if i[-1] == 'jump']
    temp = zip(chain([0], jumps), chain(jumps, [None]))
    palabras = [palabras[i:j] for i, j in temp]
    return palabras, cifras_


def get_cifras_palabras(splat_list):
    full_palabras = []
    full_cifras = []
    for r in range(len(splat_list)):
        A, B = find_ammounts(splat_list[r])
        full_palabras.append(A)
        full_cifras.append(B)
    joint_palabras = []
    for i in full_palabras:
        for j in i:
            if j != []:
                # print(strip_accents(' '.join([u[0] for u in j])))
                A = [' '.join([u[0] for u in j])]
                A.extend(j[0][1:3])
                A.extend(j[-1][3:5])
                A.extend(j[-1][5:7])
                A.extend(j[0][7:9])
                joint_palabras.append(A)
            else:
                continue
    disjointed_cifras = []
    for i in full_cifras:
        if i != []:
            disjointed_cifras.extend(i)
    return joint_palabras, disjointed_cifras


def get_campos(joint_palabras):
    campos_ = []
    for i in range(len(joint_palabras)):
        for c in campos.keys():
            for cc in campos[c]:
                if fuzz.ratio(cc, strip_accents(joint_palabras[i][0]).lower()) >= 70:
                    campos_.append(
                        [fuzz.ratio(cc, strip_accents(joint_palabras[i][0]).lower()), c, cc, joint_palabras[i]])

    # for i in range(len(joint_palabras)):
    pre_res = {}
    for c in campos.keys():
        try:
            A = [i for i in campos_ if i[1] == c]
            B = sorted(A, key=itemgetter(0, 0))[-1]
            # print(c,A,B)
            pre_res[c] = B
        except Exception as e:
            print(e)
    return pre_res


def pre_campo_valor(pre_res, disjointed_cifras):
    res = []
    for c in pre_res.keys():
        ccc = []
        # print('####', pre_res[c])
        for i in range(len(disjointed_cifras)):
            dist = (pre_res[c][-1][5] - disjointed_cifras[i][7]) ** 2 + (
                        pre_res[c][-1][6] - disjointed_cifras[i][8]) ** 2
            # rad = math.atan2(palabras[p][8]-palabras[p-1][6],palabras[p][7]-palabras[p-1][5])
            rad = math.atan2(disjointed_cifras[i][2] - pre_res[c][-1][2], disjointed_cifras[i][1] - pre_res[c][-1][1])
            ang = math.degrees(rad)
            ccc.append([c, ang, dist, disjointed_cifras[i]])
            ccc = [i for i in ccc if i[1] > -2 and 90 > i[1]]
            ccc = sorted(ccc, key=itemgetter(2, 0))
        # print(ccc[0])
        res.append([ccc[0][0], ccc[0][3][:9]])
    return res


# %%

@app.route('/extract', methods=['POST'])
def get_all():
    cv.destroyAllWindows()
    global IN
    IN = request.files['IN'].read()
    name = request.files['IN'].name
    # npimg = np.fromstring(IN, np.uint8)
    # img = cv.imdecode(npimg, cv.IMREAD_COLOR)
    global imgs
    imgs = []
    doc = fitz.open('pdf', IN)
    for i in range(len(doc)):
        for img in doc.getPageImageList(i):
            xref = img[0]
            pix = fitz.Pixmap(doc, xref)
            if pix.n < 5:  # this is GRAY or RGB
                pix.writePNG("app/ocr/temp/p%s-%s.png" % (i, xref))
                imgs.append("app/ocr/temp/p%s-%s.png" % (i, xref))
            else:  # CMYK: convert to RGB first
                pix1 = fitz.Pixmap(fitz.csRGB, pix)
                pix1.writePNG("app/ocr/temp/p%s-%s.png" % (i, xref))
                imgs.append("app/ocr/temp/p%s-%s.png" % (i, xref))
                pix1 = None
            # print(pix)
            pix = None
    # Im = cv.imread(IN)
    global data, result
    data = []
    images = []
    for img in imgs:
        image = cv.imread(img)
        images.append(image)
        data.append(get_lecture(image))

    data = data[1:]
    data = sorted(data, key=itemgetter(2, 0))
    
    splat_list = split_list(data, y_jump_index)
    joint_palabras, disjointed_cifras = get_cifras_palabras(splat_list)
    pre_res = get_campos(joint_palabras)
    res = pre_campo_valor(pre_res, disjointed_cifras)

    response = {'code': 200,
                'data': [{
                    'balance general': {'caja y efectivo': '',
                                        'total activo': '',
                                        'total pasivo': '',
                                        'total patrimonio': ''},
                    'estados perdidas y ganancias': {'costo de ventas': '',
                                                     'utilidad antes de impuestos': '',
                                                     'utilidad bruta': '',
                                                     'utilidad neta': '',
                                                     'utilidad operacional': '',
                                                     'ventas': ''},
                    'general': {'fecha': '', 'unidades de medida': ''}}],
                'message': 'success'}
    
    response['data'][0]['balance general']['caja y efectivo'] = [i for i in res if i[0].lower() == 'caja y bancos']
    response['data'][0]['balance general']['total activo'] = [i for i in res if i[0].lower() == 'total activo']
    response['data'][0]['balance general']['total pasivo'] = [i for i in res if i[0].lower() == 'total pasivo']
    response['data'][0]['balance general']['total patrimonio'] = [i for i in res if i[0].lower() == 'total patrimonio ']
    
    response['data'][0]['estados perdidas y ganancias']['costo de ventas'] = [i for i in res if i[0].lower() == 'costo de ventas']
    response['data'][0]['estados perdidas y ganancias']['utilidad antes de impuestos'] = [i for i in res if i[0].lower() == 'utilidad antes de impuestos']
    response['data'][0]['estados perdidas y ganancias']['utilidad bruta'] = [i for i in res if i[0].lower() == 'utilidad bruta']
    response['data'][0]['estados perdidas y ganancias']['utilidad neta'] = [i for i in res if i[0].lower() == 'utilidad neta']
    response['data'][0]['estados perdidas y ganancias']['utilidad operacional'] = [i for i in res if i[0].lower() == 'utilidad operacional']
    response['data'][0]['estados perdidas y ganancias']['ventas'] = [i for i in res if i[0].lower() == 'total activo']
   
    response['data'][0]['general']['fecha'] = [i for i in res if i[0].lower() == 'ventas']
    response['data'][0]['general']['unidades de medida'] = [i for i in res if i[0].lower() == 'unidades de medida']
    for i in range(len(response['data'])):
        for j in response['data'][i].keys():
            for k in response['data'][i][j].keys():
                if response['data'][i][j][k] == []:
                    response['data'][i][j][k] = ''
                else:
                    # print('notzero',response['data'][i][j][k])
                    response['data'][i][j][k] = response['data'][i][j][k][0][1]
    # print('####',res)
    cv.destroyAllWindows()

    response['name'] = name
    return response


# %%
def initOCR():
    cv.destroyAllWindows()
    aaa = time.time()
    # Read and store arguments
    # IN = 'EstadodeSituacionFinancieraal31deMarzode2015_page-0001.jpg'
    width = 320
    height = 320
    thr = 0.5
    nms = 0.4
    global y_jump_index
    y_jump_index = 0.5
    global confThreshold, nmsThreshold, inpWidth, inpHeight

    confThreshold = thr
    nmsThreshold = nms
    inpWidth = width
    inpHeight = height
    # model = args.model
    model = "app/ocr/frozen_east_text_detection.pb-master/frozen_east_text_detection.pb"

    global cifra, any_d
    cifra = re.compile('^-?\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$')
    any_d = re.compile('^\d{1,1000000}$')
    global campos
    campos = {
        "Caja y bancos": ["caja y bancos", "efectivo y equivalentes en efectivo", "efectivo y equivalentes"],
        "Total activo": ["total activo", "suma de los activos ", "activo total"],
        "Total pasivo": ["total pasivo", "suma de los pasivos", "pasivo total"],
        "Total patrimonio ": ["total patrimonio", "suma de los patrimonios", "patrimonio total", "patrimonio",
                              "suma el capital contable", "total capital contable"],
        "Ventas": ["ventas", "ventas por operación ordinacia", "ingresos por operación",
                   "ingresos por operación ordinaria", "ingresos operacionales", "ventas brutas",
                   "ingreso por actividades ordinarias"],
        "Costo de ventas": ["costo de ventas", "costos por ventas", "costo de actividades ordinarias"],
        "Utilidad Bruta": ["utilidad Bruta", "uerdida bruta"],
        "Utilidad operacional": ["utilidad operacional", "perdida operacional"],
        "Utilidad antes de impuestos": ["utilidad antes de impuestos", "perdida antes de impuestos"],
        "Utilidad neta": ["utilidad neta", "perdida neta"]
    }

    global net
    net = cv.dnn.readNet(model)
    cv.destroyAllWindows()
    cv.waitKey(0)