# Virtual Environment:

	pip install -r requirements.txt

# Run App

	set FLASK_APP=app.py (Windows)
	export FLASK_APP=app.py (Linux/MacOS)

	python app.py

# Current API Endpoints:

## http://localhost:5000/json-transformer/
**POST** request body:

```JSON
{
    "documento": {
        "balance general": {
            "caja y efectivo": "200",
            "total activo": "200",
            "total pasivo": "200",
            "total patrimonio": "400"
        },
        "estados perdidas y ganancias": {
            "ventas": "1000",
            "costo de ventas": "2000",
            "utilidad bruta": "500.987",
            "utilidad operacional": "392",
            "utilidad antes de impuestos": "9876",
            "utilidad neta": "45678"
        },
        "general": {
            "id": "1",
            "fecha": "21/12/2020",
            "unidades de medida": "MXN"
        }
    }
}
```

Expected response:
```JSON
{
    "caja y efectivo": "200",
    "costo de ventas": "2000",
    "fecha": "21/12/2020",
    "id": "1",
    "total activo": "200",
    "total pasivo": "200",
    "total patrimonio": "400",
    "unidades de medida": "MXN",
    "utilidad antes de impuestos": "9876",
    "utilidad bruta": "500.987",
    "utilidad neta": "45678",
    "utilidad operacional": "392",
    "ventas": "1000"
}
```


## http://localhost:5000/json-table/
**POST** request body:
```JSON
{
    "documento": {
        "balance general": {
            "caja y efectivo": "200",
            "total activo": "200",
            "total pasivo": "200",
            "total patrimonio": "400"
        },
        "estados perdidas y ganancias": {
            "ventas": "1000",
            "costo de ventas": "2000",
            "utilidad bruta": "500.987",
            "utilidad operacional": "392",
            "utilidad antes de impuestos": "9876",
            "utilidad neta": "45678"
        },
        "general": {
            "id": "1",
            "fecha": "21/12/2020",
            "unidades de medida": "MXN"
        }
    }
}
```
Expected response:

|id|caja y efectivo|total activo|total pasivo|total patrimonio|ventas|costo de ventas|utilidad bruta|utilidad operacional|utilidad antes de impuestos|utilidad neta|fecha|unidades de medida|
|--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |
|1|200|200|200|400|1000|2000|500.987|392|9876|45678|21/12/2020|MXN|


And will add a new row with every new **POST** request.
